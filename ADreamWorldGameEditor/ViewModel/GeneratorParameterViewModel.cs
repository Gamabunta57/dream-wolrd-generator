﻿using ADreamWorldGameEditor.Model;
using MapGridGenerator.Desktop.GridSystem;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ADreamWorldGameEditor.ViewModel
{

    class GeneratorParameterViewModel : INotifyPropertyChanged
    {
        GridGeneratorParameters gridGeneratorParameters;

        public GridGeneratorParameters Parameters
        {
            get { return gridGeneratorParameters; }
            set { gridGeneratorParameters = value; }
        }

        public bool UseCustomSeed
        {
            get { return gridGeneratorParameters.useCustomSeed; }
            set { gridGeneratorParameters.useCustomSeed = value; }
        }

        public int Seed
        {
            get { return gridGeneratorParameters.seed; }
            set { gridGeneratorParameters.seed = value; }
        }

        public int Width
        {
            get { return gridGeneratorParameters.gridWidth; }
            set { gridGeneratorParameters.gridWidth = value; }
        }

        public int Height
        {
            get { return gridGeneratorParameters.gridHeight; }
            set { gridGeneratorParameters.gridHeight = value; }
        }

        public EGeneratorOrigin OriginType
        {
            get { return gridGeneratorParameters.originType; }
            set
            {
                if (gridGeneratorParameters != null && gridGeneratorParameters.originType != value)
                {
                    gridGeneratorParameters.originType = value;
                    RaisePropertyChanged("OriginType");
                }
            }
        }

        public ObservableCollection<GeneratorStep> StepList
        {
            get { return gridGeneratorParameters.stepList; }
            set { gridGeneratorParameters.stepList = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public MapGridGenerator.Desktop.Generators.GridGeneratorParameters GetBase()
        {
            MapGridGenerator.Desktop.Generators.GridGeneratorParameters param = new MapGridGenerator.Desktop.Generators.GridGeneratorParameters()
            {
                gridHeight = Height,
                gridWidth = Width,
                originType = OriginType,
                seed = Seed,
                useCustomSeed = UseCustomSeed
            };

            MapGridGenerator.Desktop.Generators.GeneratorStep[] stepList = new MapGridGenerator.Desktop.Generators.GeneratorStep[StepList.Count];
            for (int i = 0; i < stepList.Length; i++)
            {
                stepList[i].currentAbility = StepList[i].currentAbility;
                stepList[i].probabilityToUseFullStack = StepList[i].probabilityToUseFullStack;
                stepList[i].RoomCountToGenerate = StepList[i].RoomCountToGenerate;
                stepList[i].RoomCountToGenerateBis = StepList[i].RoomCountToGenerateBis;
                stepList[i].stackOption = StepList[i].stackOption;
                stepList[i].templateRoomList = new MapGridGenerator.Desktop.GridSystem.GridRoom[StepList[i].templateRoomList.Count];

                for (int j = 0; j < stepList[i].templateRoomList.Length; j++)
                {
                    stepList[i].templateRoomList[j].roomSpaceList = new Microsoft.Xna.Framework.Point[StepList[i].templateRoomList[j].roomSpaceList.Count];
                    stepList[i].templateRoomList[j].accrocheList = new Microsoft.Xna.Framework.Point[StepList[i].templateRoomList[j].accrocheList.Count];
                    for (int k = 0; k < stepList[i].templateRoomList[j].roomSpaceList.Length; k++)
                    {
                        stepList[i].templateRoomList[j].roomSpaceList[k].X = StepList[i].templateRoomList[j].roomSpaceList[k].X;
                        stepList[i].templateRoomList[j].roomSpaceList[k].Y = StepList[i].templateRoomList[j].roomSpaceList[k].Y;
                    }

                    for (int k = 0; k < stepList[i].templateRoomList[j].accrocheList.Length; k++)
                    {
                        stepList[i].templateRoomList[j].accrocheList[k].X = StepList[i].templateRoomList[j].accrocheList[k].X;
                        stepList[i].templateRoomList[j].accrocheList[k].Y = StepList[i].templateRoomList[j].accrocheList[k].Y;
                    }
                }
            }

            param.stepList = stepList;
            return param;
        }

    }
}
