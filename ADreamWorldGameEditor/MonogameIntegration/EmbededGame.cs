﻿using MapGridGenerator.Desktop;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Framework.WpfInterop;
using MonoGame.Framework.WpfInterop.Input;

namespace ADreamWorldGameEditor.MonogameIntegration
{
    public class EmbededGame : WpfGame
    {
        private IGraphicsDeviceService _graphicsDeviceManager;
        private WpfKeyboard _keyboard;
        private WpfMouse _mouse;

        private Game1 _game;
        private Game1 game { get { if (null == _game) _game = new Game1(); return _game; } }
        
        protected override void Dispose(bool disposing)
        {
            game.Dispose();
            base.Dispose(disposing);
        }

        protected override void Initialize()
        {
            // must be initialized. required by Content loading and rendering (will add itself to the Services)
            // note that MonoGame requires this to be initialized in the constructor, while WpfInterop requires it to
            // be called inside Initialize (before base.Initialize())
            _graphicsDeviceManager = new WpfGraphicsDeviceService(this);
            // wpf and keyboard need reference to the host control in order to receive input
            // this means every WpfGame control will have it's own keyboard & mouse manager which will only react if the mouse is in the control
            _keyboard = new WpfKeyboard(this);
            _mouse = new WpfMouse(this);
            

            // must be called after the WpfGraphicsDeviceService instance was created
            base.Initialize();
            game.DoInitialize();
            // content loading now possible
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            game.DoLoadContent(_graphicsDeviceManager);
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
            game.DoUnloadContent();
        }

        protected override void Update(GameTime time)
        {
            // every update we can now query the keyboard & mouse for our WpfGame
            var mouseState = _mouse.GetState();
            var keyboardState = _keyboard.GetState();
            game.DoUpdate(time);
        }

        protected override void Draw(GameTime time)
        {
            game.DoDraw(time);
        }
    }
}
