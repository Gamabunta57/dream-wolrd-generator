﻿using ADreamWorldGameEditor.ViewModel;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace ADreamWorldGameEditor.CustomUserControl
{
    /// <summary>
    /// Logique d'interaction pour GeneratorParameters.xaml
    /// </summary>
    public partial class GeneratorParameters : UserControl
    {
        GeneratorParameterViewModel vm { get { return (GeneratorParameterViewModel)DataContext; } }

        public GeneratorParameters()
        {
            InitializeComponent();
        }

        private void Checkbox_useCustomSeed_Checked(object sender, RoutedEventArgs e)
        {
            if (null == Text_seed)
            {
                return;
            }

            Text_seed.IsEnabled = true;
            Text_seed.Focus();
        }

        private void Checkbox_useCustomSeed_Unchecked(object sender, RoutedEventArgs e)
        {
            if (null == Text_seed)
            {
                return;
            }

            Text_seed.IsEnabled = false;
        }

        private void Button_AddGeneratorStep_Click(object sender, RoutedEventArgs e)
        {
            AddNewStepElement(new Model.GeneratorStep());
        }

        private void AddNewStepElement(Model.GeneratorStep generatorStep)
        {
            vm.StepList.Add(generatorStep);
        }

        private void Button_removeStep_Click(object sender, RoutedEventArgs e)
        {
            Model.GeneratorStep step = (Model.GeneratorStep)((Button)sender).DataContext;
            vm.StepList.Remove(step);
        }

        private void Button_AddRoomTemplate_Click(object sender, RoutedEventArgs e)
        {
            Model.GeneratorStep step = (Model.GeneratorStep)((Button)sender).DataContext;
            step.templateRoomList.Add(new Model.GridRoom());
        }

        private void Button_removeTemplate_click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            ((ObservableCollection<Model.GridRoom>)(btn.CommandParameter)).Remove((Model.GridRoom)btn.DataContext);
        }
    }
}
