﻿using ADreamWorldGameEditor.ViewModel;
using MapGridGenerator.Desktop.Generators;
using MapGridGenerator.Desktop.GridSystem;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using GridGeneratorParameters = MapGridGenerator.Desktop.Generators.GridGeneratorParameters;

namespace ADreamWorldGameEditor
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainDebugViewModel vm { get { return (MainDebugViewModel)DataContext; } }
        string imageUri = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "genproc.png");

        GridGeneratorParameters parameters
        {
            get
            {
                return ((GeneratorParameterViewModel)(GridParameterControl.DataContext)).GetBase();
            }
        }

        public MainWindow()
        {
            Model.GridGeneratorParameters parameters = JsonConvert.DeserializeObject<Model.GridGeneratorParameters>(File.ReadAllText("E:/MonogameProject/MapGenerator/MapGridGenerator/data.json"));
            DataContext = new MainDebugViewModel()
            {
                imageUri = imageUri
            };
            InitializeComponent();
            GridParameterControl.DataContext = new GeneratorParameterViewModel()
            {
                Parameters = parameters
            };
        }

        private void Button_RegenerateGrid_click(object sender, RoutedEventArgs e)
        {
            RegenerateGrid();
        }

        void RegenerateGrid()
        {
            GridGeneratorParameters generatorParameters = parameters;
            generatorParameters.Init();

            var watch = System.Diagnostics.Stopwatch.StartNew();

            DreamWorldGenerator generator = new DreamWorldGenerator();
            Grid grid = generator.GenerateFrom(ref generatorParameters);

            watch.Stop();

            Console.WriteLine(watch.ElapsedMilliseconds);
            DebugGrid.CopyToFile(ref grid, imageUri, new GridPositionStack());

            RefreshPreview();
        }

        void RefreshPreview()
        {
            BitmapImage bitmapImage = new BitmapImage();//   

            bitmapImage.BeginInit();
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
            bitmapImage.UriSource = new Uri(vm.imageUri);
            bitmapImage.EndInit();

            GridPreview.Source = bitmapImage;
        }
    }
}
