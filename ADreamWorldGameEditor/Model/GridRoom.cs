﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ADreamWorldGameEditor.Model
{
    public class GridRoom : INotifyPropertyChanged
    {
        public ObservableCollection<Point> accrocheList { get; set; }
        public ObservableCollection<Point> roomSpaceList { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
