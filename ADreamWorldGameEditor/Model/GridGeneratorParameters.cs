﻿using MapGridGenerator.Desktop.GridSystem;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ADreamWorldGameEditor.Model
{
    public class GridGeneratorParameters : INotifyPropertyChanged
    {
        bool _useCustomSeed;
        int _seed;
        int _gridWidth;
        int _gridHeight;
        ObservableCollection<GeneratorStep> _stepList;
        EGeneratorOrigin _originType;

        public bool useCustomSeed { get { return _useCustomSeed; } set { if (value == _useCustomSeed) { return; } _useCustomSeed = value; RaisePropertyChanged("useCustomSeed"); } }
        public int seed { get { return _seed; } set { if (value == _seed) { return; } _seed = value; RaisePropertyChanged("seed"); } }
        public int gridWidth { get { return _gridWidth; } set { if (value == _gridWidth) { return; } _gridWidth = value; RaisePropertyChanged("gridWidth"); } }
        public int gridHeight { get { return _gridHeight; } set { if (value == _gridHeight) { return; } _gridHeight = value; RaisePropertyChanged("gridHeight"); } }
        public ObservableCollection<GeneratorStep> stepList { get { return _stepList; } set { if (value == _stepList) { return; } _stepList = value; RaisePropertyChanged("stepList"); } }
        public EGeneratorOrigin originType { get { return _originType; } set { if (value == _originType) { return; } _originType = value; RaisePropertyChanged("originType"); } }

        public int RoomCountToGenerate { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
