﻿using MapGridGenerator.Desktop.Abilities;
using MapGridGenerator.Desktop.Generators;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ADreamWorldGameEditor.Model
{
    public class GeneratorStep : INotifyPropertyChanged
    {
        int _RoomCountToGenerate;
        int _RoomCountToGenerateBis;
        ObservableCollection<GridRoom> _templateRoomList;
        EAbilities _currentAbility;
        float _probabilityToUseFullStack;
        EStackOption _stackOption;

        public int RoomCountToGenerate { get { return _RoomCountToGenerate; } set { if (value == _RoomCountToGenerate) { return; } _RoomCountToGenerate = value; RaisePropertyChanged("RoomCountToGenerate"); } }
        public int RoomCountToGenerateBis { get { return _RoomCountToGenerateBis; } set { if (value == _RoomCountToGenerateBis) { return; } _RoomCountToGenerateBis = value; RaisePropertyChanged("RoomCountToGenerateBis"); } }
        public ObservableCollection<GridRoom> templateRoomList { get { return _templateRoomList; } set { if (value == _templateRoomList) { return; } _templateRoomList = value; RaisePropertyChanged("templateRoomList"); } }
        public EAbilities currentAbility { get { return _currentAbility; } set { if (value == currentAbility) { return; } _currentAbility = value; RaisePropertyChanged("currentAbility"); } }
        public float probabilityToUseFullStack { get { return _probabilityToUseFullStack; } set { if (value == _probabilityToUseFullStack) { return; } _probabilityToUseFullStack = value; RaisePropertyChanged("probabilityToUseFullStack"); } }

        public EStackOption stackOption
        {
            get { return _stackOption; }
            set
            {
                if (_stackOption == value)
                {
                    return;
                }

                _stackOption = value;

                RaisePropertyChanged("stackOption");
                RaisePropertyChanged("isProbabilityFieldEnabled");
            }
        }

        public bool isProbabilityFieldEnabled
        {
            get { return stackOption == EStackOption.USE_BOTH_STACK; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
