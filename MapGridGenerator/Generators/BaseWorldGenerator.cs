using System;
using System.Collections.Generic;
using MapGridGenerator.Desktop.Abilities;
using MapGridGenerator.Desktop.GridSystem;
using Microsoft.Xna.Framework;

namespace MapGridGenerator.Desktop.Generators
{
	public class BaseWorldGenerator
	{
		Random random;
		int roomPlaced;
		int gridWidth;
		int gridHeight;
		int halfWidth;
		int halfHeight;
		bool stackByStep = true;

		void Init(ref GridGeneratorParameters generatorParameters)
		{
			random = new Random(generatorParameters.seed);
			roomPlaced = 0;
			gridWidth = generatorParameters.gridWidth;
			gridHeight = generatorParameters.gridHeight;
			halfWidth = (int)(gridWidth * .5f);
			halfHeight = (int)(gridHeight * .5f);
		}

		public Grid GenerateFrom(ref GridGeneratorParameters generatorParameters)
		{
			Init(ref generatorParameters);

			Grid grid = new Grid
			{
				Table = new int[generatorParameters.gridWidth, generatorParameters.gridHeight]
			};

			Generate(ref grid, ref generatorParameters);
			return grid;
		}

		void Generate(ref Grid grid, ref GridGeneratorParameters generatorParameters)
		{
			int gridLength = gridWidth * gridHeight;
			int lastRoomAbilitiesIndex = 0;
			bool isFirst = true;
			bool hasPlacedRoomLastLoop = true;
			Point[] availablePoint = new Point[4];
			EAbilities lastAbilities = generatorParameters.GetFirstAbility();
			EAbilities currentAbilitiesToAccessRoom = lastAbilities;
			GridRoom[] roomList = new GridRoom[generatorParameters.GetMaxRoomPossibilitesCount()];

			grid.RoomList = new GridRoom[generatorParameters.RoomCountToGenerate + 1]; // FIX the +1
			for (int a = 0; a < gridWidth; a++)
				for (int b = 0; b < gridHeight; b++)
					grid.Table[a, b] = -1;

			GridPositionStack stackPosition = new GridPositionStack
			{
				fullStackPosition = new List<Point>(gridLength),
				subStackPosition = new List<Point>(gridLength),
				random = random
			};

			stackPosition.Add(GetOrigin(generatorParameters.originType));

			while (stackPosition.Count > 0 && roomPlaced < generatorParameters.RoomCountToGenerate)
			{
				Point currentPositionInGrid = stackPosition.Next();
				if (hasPlacedRoomLastLoop)
					currentAbilitiesToAccessRoom = generatorParameters.NextAccessType();

				hasPlacedRoomLastLoop = true;

				if (currentAbilitiesToAccessRoom != lastAbilities)
				{
					int roomIndex = random.Next(lastRoomAbilitiesIndex, roomPlaced);
					Point coord = grid.RoomList[roomIndex].roomSpaceList[random.Next(grid.RoomList[roomIndex].roomSpaceList.Length)];
					grid.RoomList[roomIndex].abilitiesPresent = currentAbilitiesToAccessRoom;
					grid.RoomList[roomIndex].abilitiesCoordinate = coord;
					lastAbilities = currentAbilitiesToAccessRoom;
					lastRoomAbilitiesIndex = roomPlaced;
					stackPosition.SetOption(generatorParameters.StackUsageOption, ref currentPositionInGrid);
					stackPosition.FullStackUsageProbability = generatorParameters.UseFullStackProbability;
				}

				int availableRoomCount = GetAvailableRoom(grid.Table, ref currentPositionInGrid, generatorParameters.templateRoomList, roomList);
				if (availableRoomCount == 0)
				{
					hasPlacedRoomLastLoop = false;
					continue;
				}

				GridRoom room = roomList[random.Next(availableRoomCount)];
				if (!isFirst)
				{
					int roomAttachableCount = GetNextRoom(ref currentPositionInGrid, grid.Table, availablePoint);
					Door door = new Door
					{
						start = currentPositionInGrid,
						end = availablePoint[random.Next(roomAttachableCount)]
					};
					door.isTransitionnal = grid.RoomList[grid.Table[door.end.X, door.end.Y]].accessType != currentAbilitiesToAccessRoom;

					room.AddDoor(ref door);
					grid.RoomList[grid.Table[door.end.X, door.end.Y]].AddDoor(ref door);
				}

				for (int i = 0; i < room.roomSpaceList.Length; i++)
				{
					grid.Table[room.roomSpaceList[i].X, room.roomSpaceList[i].Y] = roomPlaced;
					stackPosition.RemovePoint(ref room.roomSpaceList[i]);
				}

				room.isStartingPoint = isFirst;
				room.accessType = currentAbilitiesToAccessRoom;
				grid.RoomList[roomPlaced] = room;

				isFirst = false;

				for (int i = 0; i < room.accrocheList.Length; i++)
					if (IsPointInsideCurrentGrid(ref room.accrocheList[i], grid.Table))
						stackPosition.Add(room.accrocheList[i]);

				roomPlaced++;

				DebugGrid.CopyToFile(ref grid, "genproc_" + generatorParameters.seed + "_" + roomPlaced + ".png");
			}
			Console.WriteLine("Finished");
		}

		int GetNextRoom(ref Point refPoint, int[,] Table, Point[] availablePoint)
		{
			int count = 0;

			Point top = refPoint + new Point(0, -1);
			Point bottom = refPoint + new Point(0, 1);
			Point left = refPoint + new Point(-1, 0);
			Point right = refPoint + new Point(1, 0);

			if (top.X > -1 && top.Y > -1 && top.X < gridWidth && top.Y < gridHeight && Table[top.X, top.Y] > -1)
				availablePoint[count++] = top;

			if (bottom.X > -1 && bottom.Y > -1 && bottom.X < gridWidth && bottom.Y < gridHeight && Table[bottom.X, bottom.Y] > -1)
				availablePoint[count++] = bottom;

			if (left.X > -1 && left.Y > -1 && left.X < gridWidth && left.Y < gridHeight && Table[left.X, left.Y] > -1)
				availablePoint[count++] = left;

			if (right.X > -1 && right.Y > -1 && right.X < gridWidth && right.Y < gridHeight && Table[right.X, right.Y] > -1)
				availablePoint[count++] = right;

			return count;
		}

		Point GetOrigin(EGeneratorOrigin originType)
		{
			switch (originType)
			{
				case EGeneratorOrigin.CENTER:
					{
						return new Point(
							(int)Math.Round(gridWidth * .5f),
							(int)Math.Round(gridHeight * .5f)
						);
					}
				case EGeneratorOrigin.MIX:
					{
						return new Point(
							(int)Math.Round(halfWidth * .5f + random.Next(halfWidth)),
							(int)Math.Round(halfHeight * .5f + random.Next(halfHeight))
						);

					}
				default:
					{
						return new Point(
							random.Next(gridWidth),
							random.Next(gridHeight)
						);
					}
			}
		}

		int GetAvailableRoom(int[,] Table, ref Point currentPositionInGrid, GridRoom[] templateRoomList, GridRoom[] availableRoomList)
		{
			Point worldPosition = new Point(0, 0);
			int roomCount = 0;

			for (int j = 0; j < templateRoomList.Length; j++)
			{
				GridRoom currentTemplate = templateRoomList[j];

				for (int k = 0; k < currentTemplate.roomSpaceList.Length; k++)
				{
					GridRoom roomInWorldCoordinate = currentTemplate.GetCopy();
					Point templateOffset = currentTemplate.roomSpaceList[k];
					bool allRoomAvailable = true;

					for (int i = 0; i < currentTemplate.roomSpaceList.Length; i++)
					{
						worldPosition = currentTemplate.roomSpaceList[i] + currentPositionInGrid - templateOffset;
						roomInWorldCoordinate.roomSpaceList[i] = worldPosition;

						if (!IsPointInsideCurrentGrid(ref worldPosition, Table))
						{
							allRoomAvailable = false;
							break;
						}
					}

					if (!allRoomAvailable)
						continue;

					availableRoomList[roomCount] = roomInWorldCoordinate;
					for (int l = 0; l < availableRoomList[roomCount].accrocheList.Length; l++)
						availableRoomList[roomCount].accrocheList[l] += currentPositionInGrid - templateOffset;

					roomCount++;
				}
			}
			return roomCount;
		}

		bool IsPointInsideCurrentGrid(ref Point currentPoint, int[,] Table)
		{
			return currentPoint.X >= 0
				   && currentPoint.Y >= 0
				   && currentPoint.X < gridWidth
				   && currentPoint.Y < gridHeight
				   && Table[currentPoint.X, currentPoint.Y] == -1;
		}
	}
}
