﻿namespace MapGridGenerator.Desktop.Generators
{
	public enum EStackOption
	{
		USE_ONLY_FULL_STACK,
		USE_BOTH_STACK,
		USE_ONLY_SUB_STACK
	}
}
