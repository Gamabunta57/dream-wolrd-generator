using System;
using System.Collections.Generic;
using System.Diagnostics;
using MapGridGenerator.Desktop.Abilities;
using MapGridGenerator.Desktop.GridSystem;
using Microsoft.Xna.Framework;

namespace MapGridGenerator.Desktop.Generators
{
    public class DreamWorldGenerator
    {
        private const int EMPTY_CELL = -1;
        
        Random random;
        int totalRoomPlaced;
        int gridWidth;
        int gridHeight;
        int halfWidth;
        int halfHeight;
        bool stackByStep = true;
        
        //Params generation
        private int gridLength;
        private int roomCount;
        private int stepDone;
        private Point[] availablePoint;
        private EAbilities currentAbilitiesToAccessRoom;
        private Grid grid;
        private GridRoom[] roomList;
        private GridGeneratorParameters generatorParameters;

        private GridPositionStack stackPosition;
        private Point currentPositionInGrid;

        private int[][] roomByAbilities;
        
        public Grid GenerateFrom(ref GridGeneratorParameters generatorParameters)
        {
            this.generatorParameters = generatorParameters;
            Init();

            grid = new Grid
            {
                Table = new int[gridWidth, gridHeight]
            };

            GenerateCleaner();
            return grid;
        }

        void Init()
        {
            random = new Random(generatorParameters.seed);
            totalRoomPlaced = 0;
            gridWidth = generatorParameters.gridWidth;
            gridHeight = generatorParameters.gridHeight;
            halfWidth = (int)(gridWidth * .5f);
            halfHeight = (int)(gridHeight * .5f);
            gridLength = gridWidth * gridHeight;
            roomCount = generatorParameters.RoomCountToGenerate;
            stepDone = 0;
            availablePoint = new Point[4];
            roomByAbilities = new int[generatorParameters.stepList.Length][];
        }
        void GenerateCleaner()
        {
            roomList = new GridRoom[generatorParameters.GetMaxRoomPossibilitesCount()];

            InitGrid();
            InitPositionStack(random);

            for (int i = 0; i < generatorParameters.stepList.Length; i++)
            {
                stackPosition.SetOption(generatorParameters.stepList[i].stackOption, ref currentPositionInGrid);
                stackPosition.FullStackUsageProbability = generatorParameters.stepList[i].probabilityToUseFullStack;
                stackPosition.setStep(i, false);
                roomByAbilities[i] = new int[generatorParameters.stepList[i].RoomCountToGenerate +
                                             generatorParameters.stepList[i].RoomCountToGenerateBis + 1];
                currentAbilitiesToAccessRoom = generatorParameters.stepList[i].currentAbility;
                int roomPlaced = 0;
                int roomCount = generatorParameters.stepList[i].RoomCountToGenerate;
                while (stackPosition.Count > 0 && roomPlaced < roomCount)
                {
                    if (!TryAddNewRoom(ref generatorParameters.stepList[i], i == 0 && roomPlaced == 0))
                        continue;

                    roomByAbilities[i][0] += 1;
                    roomByAbilities[i][roomByAbilities[i][0]] = totalRoomPlaced - 1;
                    roomPlaced++;
                    DebugGrid.CopyToFile(ref grid, "genproc_" + generatorParameters.seed + "_" + totalRoomPlaced + ".png", stackPosition, true);
                }

                if (i > 0)
                {
                    AddBisStep(ref generatorParameters.stepList[i-1], i -1);
                    SetAbilitiesToSearchFor(ref generatorParameters.stepList[i], i - 1);
                }
                    
                CleanUpBisStack();
            }
            
            Console.WriteLine("Finished");
        }

        void AddBisStep(ref GeneratorStep priorStep, int i)
        {
            Console.WriteLine("Add bis step");       
            stackPosition.SetOption(priorStep.stackOption, ref currentPositionInGrid);
            stackPosition.FullStackUsageProbability = priorStep.probabilityToUseFullStack;
            stackPosition.setStep(i, true);
            currentAbilitiesToAccessRoom = priorStep.currentAbility;
            int roomPlaced = 0;
            int roomCount = priorStep.RoomCountToGenerateBis;
            
            while (stackPosition.Count > 0 && roomPlaced < roomCount)
            {
                if (!TryAddNewBisRoom(ref priorStep))
                    continue;
                    
                roomByAbilities[i][0] += 1;
                roomByAbilities[i][roomByAbilities[i][0]] = totalRoomPlaced - 1;
                roomPlaced++;
                DebugGrid.CopyToFile(ref grid, "genproc_" + generatorParameters.seed + "_" + totalRoomPlaced + ".png", stackPosition, true);
            }
        }

        void CleanUpBisStack()
        {
            stackPosition.cleanUpBisAvailable();
        }
        
        bool TryAddNewRoom(ref GeneratorStep generatorStep, bool isFirst)
        {
            currentPositionInGrid = stackPosition.Next();
            
            int availableRoomCount = GetAvailableRoom(ref generatorStep, roomList);
            if(availableRoomCount == 0)
                return false;

            GridRoom room = roomList[random.Next(availableRoomCount)];
            if (!isFirst)
                PlaceDoor(ref room);

            SetGridAndEmptyStack(ref room);
            SetRoomData(ref room, isFirst);
            for (int i = 0; i < room.roomSpaceList.Length; i++)
                CheckForOrientedStack(ref room.roomSpaceList[i]);
            return true;
        }
        
        bool TryAddNewBisRoom(ref GeneratorStep generatorStep)
        {
            DoorDirection currentDirection;
            StackType stackType = stackPosition.NextBis(out currentDirection, out currentPositionInGrid);
            if (stackType == StackType.NONE)
                return false;
            
            int availableRoomCount = GetAvailableRoom(ref generatorStep, roomList);
            if(availableRoomCount == 0)
                return false;

            GridRoom room = roomList[random.Next(availableRoomCount)];
            if(stackType == StackType.BIS){
                PlaceDoorBis(ref room, currentDirection);
                SetGridAndEmptyStackBis(ref room);
                SetRoomData(ref room, false);
            }
            else
            {
                PlaceDoor(ref room, true);
                SetGridAndEmptyStack(ref room);
                SetRoomData(ref room, false);
            }
            return true;
        }

        void InitGrid()
        {
            grid.RoomList = new GridRoom[roomCount];
            for (int a = 0; a < gridWidth; a++)
                for (int b = 0; b < gridHeight; b++)
                    grid.Table[a, b] = EMPTY_CELL;
        }

        void InitPositionStack(Random random)
        {
            stackPosition = new GridPositionStack
            {
                fullStackPosition = new List<Point>(gridLength),
                subStackPosition = new List<Point>(gridLength),
                bisStepStackPositionEmptyTop = new List<Point>(),
                bisStepStackPositionEmptyBottom= new List<Point>(),
                bisStepStackPositionEmptyLeft= new List<Point>(),
                bisStepStackPositionEmptyRight= new List<Point>(),
		
                bisStepStackPositionAvailableTop= new List<Point>(),
                bisStepStackPositionAvailableBottom= new List<Point>(),
                bisStepStackPositionAvailableLeft= new List<Point>(),
                bisStepStackPositionAvailableRight= new List<Point>(),
                
                bisStackPosition = new List<Point>(),
                
                stackByStep = new List<List<Point>>{new List<Point>()},

                random = random
            };
            stackPosition.Add(GetOrigin());
        }

        void SetAbilitiesToSearchFor(ref GeneratorStep generatorNextStep, int i)
        {
            int roomIndex = roomByAbilities[i][random.Next(0, roomByAbilities[i][0]) + 1];
            Point coord = grid.RoomList[roomIndex].roomSpaceList[random.Next(grid.RoomList[roomIndex].roomSpaceList.Length)];
            grid.RoomList[roomIndex].abilitiesPresent = generatorNextStep.currentAbility;
            grid.RoomList[roomIndex].abilitiesCoordinate = coord;
        }

        void PlaceDoor(ref GridRoom room, bool forceSameRoomType = false)
        {
            int roomAttachableCount = GetNextRoom(forceSameRoomType);
            Door door = new Door
            {
                start = currentPositionInGrid,
                end = availablePoint[random.Next(roomAttachableCount)],
                isSet = true
            };
            door.direction = Door.GetDirection(ref door.start, ref door.end);
            door.isTransitionnal = grid.RoomList[grid.Table[door.end.X, door.end.Y]].accessType != currentAbilitiesToAccessRoom;
            room.AddDoor(ref door);
            grid.RoomList[grid.Table[door.end.X, door.end.Y]].AddReversedDoor(ref door);
         }
        
        void PlaceDoorBis(ref GridRoom room, DoorDirection direction)
        {
            Point target =  stackPosition.GetAvailableDoorWithDirection(direction);
            Door door = new Door
            {
                start = currentPositionInGrid,
                end = target,
                direction = direction,
                isSet = true
            };
            door.isVirtual = true;

            room.AddDoor(ref door);
            grid.RoomList[grid.Table[door.end.X, door.end.Y]].AddReversedDoor(ref door);
        }

        void SetGridAndEmptyStack(ref GridRoom room)
        {
            for (int i = 0; i < room.roomSpaceList.Length; i++)
            {
                grid.Table[room.roomSpaceList[i].X, room.roomSpaceList[i].Y] = totalRoomPlaced;
                stackPosition.RemovePoint(ref room.roomSpaceList[i]);
            }
        }       
        
        void SetGridAndEmptyStackBis(ref GridRoom room)
        {
            for (int i = 0; i < room.roomSpaceList.Length; i++)
            {
                grid.Table[room.roomSpaceList[i].X, room.roomSpaceList[i].Y] = totalRoomPlaced;
                stackPosition.RemovePoint(ref room.roomSpaceList[i]);
            }
        }

        void CheckForOrientedStack(ref Point pointInGrid)
        {
            Point top = pointInGrid + new Point(0, -1);
            Point bottom = pointInGrid + new Point(0, 1);
            Point left = pointInGrid + new Point(-1, 0);
            Point right = pointInGrid + new Point(1, 0);
            
            if (top.X > -1 && top.Y > -1 && top.X < gridWidth && top.Y < gridHeight)
            {
                if (grid.Table[top.X, top.Y] == EMPTY_CELL)
                    stackPosition.AddBottomDirectionDoorEmpty(ref top);    
                else if (grid.RoomList[grid.Table[top.X, top.Y]].accessType != currentAbilitiesToAccessRoom)
                    stackPosition.AddBottomDirectionDoorAvailable(ref top);    
            }

            if (bottom.X > -1 && bottom.Y > -1 && bottom.X < gridWidth && bottom.Y < gridHeight)
            {
                if (grid.Table[bottom.X, bottom.Y] == EMPTY_CELL)
                    stackPosition.AddTopDirectionDoorEmpty(ref bottom);    
                else if (grid.RoomList[grid.Table[bottom.X, bottom.Y]].accessType != currentAbilitiesToAccessRoom)
                    stackPosition.AddTopDirectionDoorAvailable(ref bottom);
            }

            if (left.X > -1 && left.Y > -1 && left.X < gridWidth && left.Y < gridHeight)
            {
                if (grid.Table[left.X, left.Y] == EMPTY_CELL)
                    stackPosition.AddRightDirectionDoorEmpty(ref left);    
                else if (grid.RoomList[grid.Table[left.X, left.Y]].accessType != currentAbilitiesToAccessRoom)
                    stackPosition.AddRightDirectionDoorAvailable(ref left);  
            }

            if (right.X > -1 && right.Y > -1 && right.X < gridWidth && right.Y < gridHeight)
            {
                if (grid.Table[right.X, right.Y] == EMPTY_CELL)
                    stackPosition.AddLeftDirectionDoorEmpty(ref right);
                else if (grid.RoomList[grid.Table[right.X, right.Y]].accessType != currentAbilitiesToAccessRoom)
                    stackPosition.AddLeftDirectionDoorAvailable(ref right);
            }
        }

        void SetRoomData(ref GridRoom room, bool isFirst)
        {
            room.isStartingPoint = isFirst;
            room.accessType = currentAbilitiesToAccessRoom;
            grid.RoomList[totalRoomPlaced] = room;

            for (int i = 0; i < room.accrocheList.Length; i++)
                if (IsPointInsideCurrentGrid(ref room.accrocheList[i]))
                    stackPosition.Add(room.accrocheList[i]);

            totalRoomPlaced++;
        }

        int GetNextRoom(bool forceSameRoomType = false)
        {
            int count = 0;

            Point top = currentPositionInGrid + new Point(0, -1);
            Point bottom = currentPositionInGrid + new Point(0, 1);
            Point left = currentPositionInGrid + new Point(-1, 0);
            Point right = currentPositionInGrid + new Point(1, 0);

            if (top.X > -1 && top.Y > -1 && top.X < gridWidth && top.Y < gridHeight && grid.Table[top.X, top.Y] > -1
             && (!forceSameRoomType || (forceSameRoomType && currentAbilitiesToAccessRoom == grid.RoomList[grid.Table[top.X, top.Y]].accessType))
                )
               availablePoint[count++] = top;
                
            if (bottom.X > -1 && bottom.Y > -1 && bottom.X < gridWidth && bottom.Y < gridHeight && grid.Table[bottom.X, bottom.Y] > -1
                && (!forceSameRoomType || (forceSameRoomType && currentAbilitiesToAccessRoom == grid.RoomList[grid.Table[bottom.X, bottom.Y]].accessType))
            )
                availablePoint[count++] = bottom;

            if (left.X > -1 && left.Y > -1 && left.X < gridWidth && left.Y < gridHeight && grid.Table[left.X, left.Y] > -1
                && (!forceSameRoomType || (forceSameRoomType && currentAbilitiesToAccessRoom == grid.RoomList[grid.Table[left.X, left.Y]].accessType))
            )
                availablePoint[count++] = left;

            if (right.X > -1 && right.Y > -1 && right.X < gridWidth && right.Y < gridHeight && grid.Table[right.X, right.Y] > -1
                && (!forceSameRoomType || (forceSameRoomType && currentAbilitiesToAccessRoom == grid.RoomList[grid.Table[right.X, right.Y]].accessType))
            )
                availablePoint[count++] = right;

            return count;
        }

        Point GetOrigin()
        {
            switch (generatorParameters.originType)
            {
                case EGeneratorOrigin.CENTER:
                    {
                        return new Point(
                            (int)Math.Round(gridWidth * .5f),
                            (int)Math.Round(gridHeight * .5f)
                        );
                    }
                case EGeneratorOrigin.MIX:
                    {
                        return new Point(
                            (int)Math.Round(halfWidth * .5f + random.Next(halfWidth)),
                            (int)Math.Round(halfHeight * .5f + random.Next(halfHeight))
                        );

                    }
                default:
                    {
                        return new Point(
                            random.Next(gridWidth),
                            random.Next(gridHeight)
                        );
                    }
            }
        }

        int GetAvailableRoom(ref GeneratorStep generatorStep, GridRoom[] availableRoomList)
        {
            Point worldPosition = new Point(0, 0);
            int roomCount = 0;

            for (int j = 0; j < generatorStep.templateRoomList.Length; j++)
            {
                GridRoom currentTemplate = generatorStep.templateRoomList[j];

                for (int k = 0; k < currentTemplate.roomSpaceList.Length; k++)
                {
                    GridRoom roomInWorldCoordinate = currentTemplate.GetCopy();
                    Point templateOffset = currentTemplate.roomSpaceList[k];
                    bool allRoomAvailable = true;

                    for (int i = 0; i < currentTemplate.roomSpaceList.Length; i++)
                    {
                        worldPosition = currentTemplate.roomSpaceList[i] + currentPositionInGrid - templateOffset;
                        roomInWorldCoordinate.roomSpaceList[i] = worldPosition;

                        if (!IsPointInsideCurrentGrid(ref worldPosition))
                        {
                            allRoomAvailable = false;
                            break;
                        }
                    }

                    if (!allRoomAvailable)
                        continue;

                    availableRoomList[roomCount] = roomInWorldCoordinate;
                    for (int l = 0; l < availableRoomList[roomCount].accrocheList.Length; l++)
                        availableRoomList[roomCount].accrocheList[l] += currentPositionInGrid - templateOffset;

                    roomCount++;
                }
            }
            return roomCount;
        }

        bool IsPointInsideCurrentGrid(ref Point currentPoint)
        {
            return currentPoint.X >= 0
                   && currentPoint.Y >= 0
                   && currentPoint.X < gridWidth
                   && currentPoint.Y < gridHeight
                   && grid.Table[currentPoint.X, currentPoint.Y] == EMPTY_CELL;
        }
    }
}
