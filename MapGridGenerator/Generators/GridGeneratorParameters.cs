﻿using MapGridGenerator.Desktop.Abilities;
using MapGridGenerator.Desktop.GridSystem;

namespace MapGridGenerator.Desktop.Generators
{
	public struct GridGeneratorParameters
	{
        public bool useCustomSeed;
		public int seed;
		public int gridWidth;
		public int gridHeight;
		public GeneratorStep[] stepList;
		public EGeneratorOrigin originType;
		public int RoomCountToGenerate { get; private set; }

		public void Init()
		{
			RoomCountToGenerate = 0;
			for (int i = 0; i < stepList.Length; i++)
				RoomCountToGenerate += stepList[i].RoomCountToGenerate +stepList[i].RoomCountToGenerateBis;
		}

		public int GetMaxRoomPossibilitesCount()
		{
			int count = 0;
			for (int i = 0; i < stepList.Length; i++)
			{
				int currentCount = 0;
				for (int j = 0; j < stepList[i].templateRoomList.Length; j++)
					currentCount += stepList[i].templateRoomList[j].roomSpaceList.Length;

				if (currentCount > count)
					count = currentCount;
			}
			return count;
		}
	}
}
