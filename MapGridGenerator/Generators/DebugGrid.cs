﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using MapGridGenerator.Desktop.Abilities;
using MapGridGenerator.Desktop.GridSystem;

namespace MapGridGenerator.Desktop.Generators
{
	public static class DebugGrid
	{
		static int Size = 20;

		static Pen penStart = new Pen(Color.CornflowerBlue);
		static Pen penGrid = new Pen(Color.DimGray);
		static Pen penGridSmall = new Pen(Color.FromArgb(143,137,66));
		static Pen penLine = new Pen(Color.Black);
		static Pen penDoor = new Pen(Color.GreenYellow, 2);
		static Pen penVirtualDoor = new Pen(Color.FromArgb(124,130,250), 2);
		static Pen penDoorTransition = new Pen(Color.Gray, 2);
		static Pen penAbilityNone = new Pen(Color.Purple);
		static Pen penAbilityFirst = new Pen(Color.Aqua);
		static Pen penAbilitySecond = new Pen(Color.BlueViolet);
		static Pen penAbilityThird = new Pen(Color.DarkSeaGreen);
		static Pen penAbilityFourth = new Pen(Color.OrangeRed);
		static Pen penStackEmpty = new Pen(Color.Gold,2);
		static Pen penStackAvailable = new Pen(Color.Crimson,3);

		public static void CopyToFile(ref Grid grid, string filename, GridPositionStack stack, bool showStack = false)
		{
			penStackEmpty.DashCap = DashCap.Round;
			penStackAvailable.DashCap = DashCap.Triangle;
			using (Bitmap b = new Bitmap(grid.Table.GetLength(0) * Size + 1, grid.Table.GetLength(1) * Size + 1))
			{
				using (Graphics g = Graphics.FromImage(b))
				{
                    DrawInGraphics(g, ref grid, stack, showStack);
				}
				b.Save(filename, ImageFormat.Png);
			}
		}

        public static void DrawInGraphics(Graphics g, ref Grid grid, GridPositionStack stack, bool showStack = false)
        {
            g.Clear(Color.Black);
            DisplayGrid(ref grid, g);
            foreach (GridRoom gr in grid.RoomList)
            {
                if (null == gr.roomSpaceList)
                    continue;

                for (int i = 0; i < gr.roomSpaceList.Length; i++)
                {
                    Microsoft.Xna.Framework.Point p = gr.roomSpaceList[i];
                    g.FillRectangle(gr.isStartingPoint ? penStart.Brush : GetPen(gr.accessType).Brush, p.X * Size, p.Y * Size, Size, Size);

                    if (!hasTop(p, gr.roomSpaceList))
                        DrawLineTop(g, penLine, p);
                    if (!hasLeft(p, gr.roomSpaceList))
                        DrawLineLeft(g, penLine, p);
                    if (!hasBottom(p, gr.roomSpaceList))
                        DrawLineBottom(g, penLine, p);
                    if (!hasRight(p, gr.roomSpaceList))
                        DrawLineRight(g, penLine, p);
                }

                if (gr.abilitiesPresent != EAbilities.NONE)
                {
                    g.FillEllipse(GetPen(gr.abilitiesPresent).Brush,
                        gr.abilitiesCoordinate.X * Size + (int)(Size * .25f) + 1,
                        gr.abilitiesCoordinate.Y * Size + (int)(Size * .25f) + 1,
                          (int)(Size * .5f),
                          (int)(Size * .5f)
                     );
                }

                for (int i = 0; i < gr.doorCount; i++)
                {
                    if (!gr.doorList[i].isVirtual)
                        DrawDoor(g, gr.doorList[i].isTransitionnal ? penDoorTransition : penDoor, gr.doorList[i]);
                }

            }

            for (int i = 0; i < grid.RoomList.Length; i++)
            {
                for (int j = 0; j < grid.RoomList[i].doorCount; j++)
                {
                    if (grid.RoomList[i].doorList[j].isVirtual)
                        DrawVirtualDoor(g, grid.RoomList[i].doorList[j]);
                }
            }

            if (showStack)
            {
                for (int i = 0; i < stack.bisStepStackPositionAvailableBottom.Count; i++)
                    DrawPoint(g, stack.bisStepStackPositionAvailableBottom[i], DoorDirection.DOWN);
                for (int i = 0; i < stack.bisStepStackPositionAvailableTop.Count; i++)
                    DrawPoint(g, stack.bisStepStackPositionAvailableTop[i], DoorDirection.UP);
                for (int i = 0; i < stack.bisStepStackPositionAvailableLeft.Count; i++)
                    DrawPoint(g, stack.bisStepStackPositionAvailableLeft[i], DoorDirection.LEFT);
                for (int i = 0; i < stack.bisStepStackPositionAvailableRight.Count; i++)
                    DrawPoint(g, stack.bisStepStackPositionAvailableRight[i], DoorDirection.RIGHT);

                for (int i = 0; i < stack.bisStepStackPositionEmptyBottom.Count; i++)
                    DrawEmptyPoint(g, stack.bisStepStackPositionEmptyBottom[i], DoorDirection.DOWN);
                for (int i = 0; i < stack.bisStepStackPositionEmptyTop.Count; i++)
                    DrawEmptyPoint(g, stack.bisStepStackPositionEmptyTop[i], DoorDirection.UP);
                for (int i = 0; i < stack.bisStepStackPositionEmptyLeft.Count; i++)
                    DrawEmptyPoint(g, stack.bisStepStackPositionEmptyLeft[i], DoorDirection.LEFT);
                for (int i = 0; i < stack.bisStepStackPositionEmptyRight.Count; i++)
                    DrawEmptyPoint(g, stack.bisStepStackPositionEmptyRight[i], DoorDirection.RIGHT);
            }
        }

		static void DrawPoint(Graphics g, Microsoft.Xna.Framework.Point p, DoorDirection direction)
		{
			Microsoft.Xna.Framework.Point offset = new Microsoft.Xna.Framework.Point((int)(Size * .5f));
			p.X *= Size;
			p.Y *= Size;
			p += offset;
			
			if (direction == DoorDirection.DOWN)
				p.Y += offset.Y;
			else if (direction == DoorDirection.UP)
				p.Y -= offset.Y;
			else if (direction == DoorDirection.LEFT)
				p.X -= offset.X;
			else if (direction == DoorDirection.RIGHT)
				p.X += offset.X;

			g.DrawRectangle(penStackAvailable, new Rectangle(p.X,p.Y,(int)penStackAvailable.Width,(int)penStackAvailable.Width));
		}

		static void DrawEmptyPoint(Graphics g, Microsoft.Xna.Framework.Point p, DoorDirection direction)
		{
			Microsoft.Xna.Framework.Point offset = new Microsoft.Xna.Framework.Point((int)(Size * .5f));
			p.X *= Size;
			p.Y *= Size;
			p += offset;
			
			if (direction == DoorDirection.DOWN)
				p.Y += offset.Y;
			else if (direction == DoorDirection.UP)
				p.Y -= offset.Y;
			else if (direction == DoorDirection.LEFT)
				p.X -= offset.X;
			else if (direction == DoorDirection.RIGHT)
				p.X += offset.X;

			g.DrawEllipse(penStackEmpty, new Rectangle(p.X,p.Y,(int)penStackAvailable.Width,(int)penStackAvailable.Width));	
		}

		static void DisplayGrid(ref Grid grid, Graphics g)
		{
			int width = grid.Table.GetLength(0);
			int height = grid.Table.GetLength(1);
			for (int i = 0; i < width + 1; i++)
			{
				g.DrawLine(i%5 == 0 ? penGridSmall : penGrid,
					i*Size,
					0,
					i*Size,
					height*Size
				);
			}
			for (int i = 0; i < height +1; i++)
			{
				g.DrawLine(
					i%5 == 0 ? penGridSmall : penGrid,
					0,
					i*Size,
					width * Size,
					i*Size
				);
			}
		}

		static bool hasTop(Microsoft.Xna.Framework.Point point, Microsoft.Xna.Framework.Point[] roomShape)
		{
			for (int i = 0; i < roomShape.Length; i++)
			{
				if (roomShape[i].Y == point.Y - 1)
					return true;
			}
			return false;
		}

		static bool hasLeft(Microsoft.Xna.Framework.Point point, Microsoft.Xna.Framework.Point[] roomShape)
		{
			for (int i = 0; i < roomShape.Length; i++)
			{
				if (roomShape[i].X == point.X - 1)
					return true;
			}
			return false;
		}


		static bool hasBottom(Microsoft.Xna.Framework.Point point, Microsoft.Xna.Framework.Point[] roomShape)
		{
			for (int i = 0; i < roomShape.Length; i++)
			{
				if (roomShape[i].Y == point.Y + 1)
					return true;
			}
			return false;
		}

		static bool hasRight(Microsoft.Xna.Framework.Point point, Microsoft.Xna.Framework.Point[] roomShape)
		{
			for (int i = 0; i < roomShape.Length; i++)
			{
				if (roomShape[i].X == point.X + 1)
					return true;
			}
			return false;
		}

		static void DrawLineTop(Graphics graphics, Pen pen, Microsoft.Xna.Framework.Point point)
		{
			graphics.DrawLine(pen, point.X * Size, point.Y * Size, (point.X + 1) * Size, point.Y * Size);
		}

		static void DrawLineBottom(Graphics graphics, Pen pen, Microsoft.Xna.Framework.Point point)
		{
			graphics.DrawLine(pen, point.X * Size, (point.Y + 1) * Size, (point.X + 1) * Size, (point.Y + 1) * Size);
		}

		static void DrawLineLeft(Graphics graphics, Pen pen, Microsoft.Xna.Framework.Point point)
		{
			graphics.DrawLine(pen, point.X * Size, point.Y * Size, point.X * Size, (point.Y + 1) * Size);
		}

		static void DrawLineRight(Graphics graphics, Pen pen, Microsoft.Xna.Framework.Point point)
		{
			graphics.DrawLine(pen, (point.X + 1) * Size, point.Y * Size, (point.X + 1) * Size, (point.Y + 1) * Size);
		}

		static void DrawDoor(Graphics graphics, Pen pen, Door d)
		{
			int doorLength = (int)(Size * .2f);

			Microsoft.Xna.Framework.Point offset = new Microsoft.Xna.Framework.Point((int)(Size * .5f));

			Microsoft.Xna.Framework.Point start = new Microsoft.Xna.Framework.Point(d.start.X * Size, d.start.Y * Size);
			Microsoft.Xna.Framework.Point end = start;

			if (d.direction == DoorDirection.DOWN)
			{
				start.Y += offset.Y - doorLength;
				end.Y += offset.Y + doorLength + 1;
			}

			else if (d.direction == DoorDirection.UP)
			{
				start.Y -= offset.Y + doorLength;
				end.Y -= offset.Y - doorLength - 1;
			}
			else if (d.direction == DoorDirection.LEFT)
			{
				start.X -= offset.X + doorLength;
				end.X -= offset.X - doorLength - 1;
			}
			else if (d.direction == DoorDirection.RIGHT)
			{
				start.X += offset.X - doorLength;
				end.X += offset.X + doorLength + 1;
			}

			pen.DashCap = DashCap.Flat;
			graphics.DrawLine(pen,
							  start.X + offset.X,
							  start.Y + offset.Y,
							  end.X + offset.X,
							  end.Y + offset.Y
							 );

			pen.DashCap = DashCap.Triangle;
			graphics.DrawLine(pen, end.X, end.Y, end.X, end.Y);
		}
		
		static void DrawVirtualDoor(Graphics graphics, Door d)
		{	
			int doorLength = (int)(Size * .2f);
			
			Microsoft.Xna.Framework.Point offset = new Microsoft.Xna.Framework.Point((int)(Size * .5f));
			
			Microsoft.Xna.Framework.Point start = new Microsoft.Xna.Framework.Point(d.start.X * Size, d.start.Y * Size) + offset;
			Microsoft.Xna.Framework.Point endingStartDoor = start;
			
			Microsoft.Xna.Framework.Point end = new Microsoft.Xna.Framework.Point(d.end.X * Size, d.end.Y * Size) + offset;
			Microsoft.Xna.Framework.Point endingEndDoor = end;
				
			if (d.direction == DoorDirection.DOWN)
			{
				start.Y += offset.Y - doorLength;
				endingStartDoor.Y += offset.Y + doorLength + 1;

				start.X += (int)(offset.X * .5f);
				endingStartDoor.X += (int)(offset.X * .5f);
				
				end.Y -= offset.Y + doorLength;
				endingEndDoor.Y -= offset.Y - doorLength - 1;

				end.X += (int) (offset.X * .5f);
				endingEndDoor.X += (int)(offset.X * .5f);
			}

			else if (d.direction == DoorDirection.UP)
			{
				start.Y -= offset.Y + doorLength;
				endingStartDoor.Y -= offset.Y - doorLength - 1;

				start.X += (int)(offset.X * .5f);
				endingStartDoor.X += (int)(offset.X * .5f);
				
				end.Y += offset.Y - doorLength;
				endingEndDoor.Y += offset.Y + doorLength + 1;

				end.X += (int) (offset.X * .5f);
				endingEndDoor.X += (int)(offset.X * .5f);
			}
			else if (d.direction == DoorDirection.LEFT)
			{
				start.X -= offset.X + doorLength;
				endingStartDoor.X -= offset.X - doorLength - 1;

				start.Y += (int) (offset.Y * .5f);
				endingStartDoor.Y += (int) (offset.Y * .5f);
				
				end.X += offset.X - doorLength;
				endingEndDoor.X += offset.X + doorLength + 1;

				end.Y += (int) (offset.Y * .5f);
				endingEndDoor.Y += (int) (offset.Y * .5f);
			}
			else if (d.direction == DoorDirection.RIGHT)
			{
				start.X += offset.X - doorLength;
				endingStartDoor.X += offset.X + doorLength + 1;

				start.Y += (int) (offset.Y * .5f);
				endingStartDoor.Y += (int) (offset.Y * .5f);
				
				end.X -= offset.X + doorLength;
				endingEndDoor.X -= offset.X - doorLength -1;

				end.Y += (int) (offset.Y * .5f);
				endingEndDoor.Y += (int) (offset.Y * .5f);
			}

			penVirtualDoor.DashCap = DashCap.Triangle;
			penVirtualDoor.DashStyle = DashStyle.Solid;
			graphics.DrawLine(penVirtualDoor,
				start.X,
				start.Y,
				endingStartDoor.X,
				endingStartDoor.Y
			);
			
			graphics.DrawLine(penVirtualDoor,
				end.X,
				end.Y,
				endingEndDoor.X,
				endingEndDoor.Y
			);
			
			
			penVirtualDoor.DashCap = DashCap.Flat;
			penVirtualDoor.DashStyle = DashStyle.Dash;
			graphics.DrawLine(penVirtualDoor,
				start.X,
				start.Y,
				end.X,
				end.Y
			);
		}

		static Pen GetPen(EAbilities ability)
		{
			switch (ability)
			{
				case EAbilities.FIRST_ABILITY:
					return penAbilityFirst;
				case EAbilities.SECOND_ABILITY:
					return penAbilitySecond;
				case EAbilities.THIRD_ABILITY:
					return penAbilityThird;
				case EAbilities.FOURTH_ABILITY:
					return penAbilityFourth;
				default:
					return penAbilityNone;
			}
		}
	}
}
