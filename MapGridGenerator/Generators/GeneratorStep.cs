﻿using MapGridGenerator.Desktop.Abilities;
using MapGridGenerator.Desktop.GridSystem;

namespace MapGridGenerator.Desktop.Generators
{
    public struct GeneratorStep
    {
        public int RoomCountToGenerate;
        public int RoomCountToGenerateBis;
        public GridRoom[] templateRoomList;
        public EAbilities currentAbility;
        public float probabilityToUseFullStack;
        public EStackOption stackOption;
    }
}
