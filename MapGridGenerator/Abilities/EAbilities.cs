﻿using System;
namespace MapGridGenerator.Desktop.Abilities
{
	public enum EAbilities
	{
		NONE,
		FIRST_ABILITY,
		SECOND_ABILITY,
		THIRD_ABILITY,
		FOURTH_ABILITY
	}
}
