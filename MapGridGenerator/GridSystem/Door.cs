﻿using System;
using Microsoft.Xna.Framework;
namespace MapGridGenerator.Desktop.GridSystem
{
	public struct Door
	{
		public bool isSet;
		public Point start;
		public Point end;
		public bool isTransitionnal;
		public bool isVirtual;
		public Point VirtualEnd;
		public DoorDirection direction;

		public static DoorDirection GetOppositeDirection(DoorDirection direction)
		{
			if (direction == DoorDirection.UP)
				return DoorDirection.DOWN;
			
			if (direction == DoorDirection.DOWN)
				return DoorDirection.UP;

			if (direction == DoorDirection.LEFT)
				return DoorDirection.RIGHT;

			return DoorDirection.LEFT;
		}
        
		public static DoorDirection GetDirection(ref Point start, ref Point end)
		{
			if (start.Y > end.Y)
				return DoorDirection.UP;
			if (start.Y < end.Y)
				return DoorDirection.DOWN;
			if (start.X > end.X)
				return DoorDirection.LEFT;
			return DoorDirection.RIGHT;
		}
	}
}
