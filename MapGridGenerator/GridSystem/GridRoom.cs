﻿using System;
using Microsoft.Xna.Framework;
using MapGridGenerator.Desktop.Abilities;
namespace MapGridGenerator.Desktop.GridSystem
{
	public struct GridRoom
	{
		public bool isStartingPoint;
		public int doorCount;
		public Point[] accrocheList;
		public Point[] roomSpaceList;
		public Door[] doorList;
		public EAbilities accessType;
		public EAbilities abilitiesPresent;
		public Point abilitiesCoordinate;

		public GridRoom GetCopy()
		{
			GridRoom newStruct = this;
			newStruct.accrocheList = new Point[accrocheList.Length];
			newStruct.roomSpaceList = new Point[roomSpaceList.Length];
			Array.Copy(accrocheList, newStruct.accrocheList, accrocheList.Length);
			Array.Copy(roomSpaceList, newStruct.roomSpaceList, roomSpaceList.Length);
			return newStruct;
		}

		public override string ToString()
		{
			return "roomSpace : " + PointListToString(roomSpaceList) + "/ accroche : " + PointListToString(accrocheList);
		}

		string PointListToString(Point[] pointList)
		{
			string str = "";
			for (int i = 0; i < pointList.Length; i++)
				str += pointList[i] + " ";
			return str;
		}

		public void AddDoor(ref Door door)
		{
			if (null == doorList)
				doorList = new Door[2 + roomSpaceList.Length * 2];

			int indexExistingDoor;
			
			if (!GetExistingDoor(ref door, out indexExistingDoor))
				doorList[doorCount++] = door;
			else
			{
				doorList[indexExistingDoor].isVirtual = true;
				doorList[indexExistingDoor].VirtualEnd = door.end;
			}
		}

		public void AddReversedDoor(ref Door door)
		{
			Door d = new Door()
			{
				start = door.end,
				end = door.start,
				direction = Door.GetOppositeDirection(door.direction)
			};
			AddDoor(ref d);
		}

		bool GetExistingDoor(ref Door door, out int index)
		{
			for (int i = 0; i < doorList.Length; i++)
			{
				if (door.direction == doorList[i].direction && door.start == doorList[i].start)
				{
					index = i;
					return true;	
				} 
			}

			index = -1;
			return false;
		}
	}
}
