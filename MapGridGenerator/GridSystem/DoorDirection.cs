﻿using System;
namespace MapGridGenerator.Desktop.GridSystem
{
	public enum DoorDirection
	{
		UNDEFINED,
		UP,
		DOWN,
		LEFT,
		RIGHT // doit etre le dernier de la car utilise dans un random.next()
	}
}
