﻿namespace MapGridGenerator.Desktop.GridSystem
{
	public struct Grid
	{
		public int[,] Table;
		public GridRoom[] RoomList;
	}
}
