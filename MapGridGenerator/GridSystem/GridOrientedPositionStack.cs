using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace MapGridGenerator.Desktop.GridSystem
{
    public struct GridOrientedPositionStack
    {
        public List<Point> rightDoorStackLinkablePosition;
        public List<Point> leftDoorStackLinkablePosition;
        public List<Point> topDoorStackLinkablePosition;
        public List<Point> bottomDoorStackLinkablePosition;
        
        public List<Point> rightDoorStackEmptyPosition;
        public List<Point> leftDoorStackEmptyPosition;
        public List<Point> topDoorStackEmptyPosition;
        public List<Point> bottomDoorStackEmptyPosition;

        public void RemovePoint(ref Point coordinateToRemove)
        {
            int index = rightDoorStackEmptyPosition.IndexOf(coordinateToRemove);
            if (index > -1)
                rightDoorStackEmptyPosition.RemoveAt(index);

            index = leftDoorStackEmptyPosition.IndexOf(coordinateToRemove);
            if (index > -1)
                leftDoorStackEmptyPosition.RemoveAt(index);

            index = topDoorStackEmptyPosition.IndexOf(coordinateToRemove);
            if (index > -1)
                topDoorStackEmptyPosition.RemoveAt(index);

            index = bottomDoorStackEmptyPosition.IndexOf(coordinateToRemove);
            if (index > -1)
                bottomDoorStackEmptyPosition.RemoveAt(index);
        }

        public void AddTopDirectionDoorEmpty(ref Point point)
        {
            topDoorStackEmptyPosition.Add(point);
        }

        public void AddBottomDirectionDoorEmpty(ref Point point)
        {
            bottomDoorStackEmptyPosition.Add(point);
        }

        public void AddLeftDirectionDoorEmpty(ref Point point)
        {
            leftDoorStackEmptyPosition.Add(point);
        }

        public void AddRightDirectionDoorEmpty(ref Point point)
        {
            rightDoorStackEmptyPosition.Add(point);
        }

        public void AddTopDirectionDoorAvailable(ref Point point)
        {
            topDoorStackLinkablePosition.Add(point);
        }
        
        public void AddBottomDirectionDoorAvailable(ref Point point)
        {
            bottomDoorStackLinkablePosition.Add(point);
        }
        
        public void AddLeftDirectionDoorAvailable(ref Point point)
        {
            leftDoorStackLinkablePosition.Add(point);
        }
        
        public void AddRightDirectionDoorAvailable(ref Point point)
        {
            rightDoorStackLinkablePosition.Add(point);
        }

    }
}