﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using MapGridGenerator.Desktop.Generators;
namespace MapGridGenerator.Desktop.GridSystem
{
	public struct GridPositionStack
	{
		public List<Point> fullStackPosition;
		public List<Point> subStackPosition;
		public List<Point> bisStackPosition;

		
		public List<List<Point>> stackByStep;
		int currentStep;
		bool isBisStep;
		
		public Random random;
		public float FullStackUsageProbability;

		bool useSubstack { get { return option != EStackOption.USE_ONLY_FULL_STACK; } }
		public int Count { get { return fullStackPosition.Count; } }

		public int BisCount {get { return GetBisCount(); }}

		public List<Point> bisStepStackPositionEmptyTop;
		public List<Point> bisStepStackPositionEmptyBottom;
		public List<Point> bisStepStackPositionEmptyLeft;
		public List<Point> bisStepStackPositionEmptyRight;
		
		public List<Point> bisStepStackPositionAvailableTop;
		public List<Point> bisStepStackPositionAvailableBottom;
		public List<Point> bisStepStackPositionAvailableLeft;
		public List<Point> bisStepStackPositionAvailableRight;
		
		EStackOption option;

		public void Add(Point pointToAdd)
		{
			if (!PointExistInStack(ref pointToAdd, fullStackPosition))
				fullStackPosition.Add(pointToAdd);

			if (useSubstack && !PointExistInStack(ref pointToAdd, subStackPosition))
				subStackPosition.Add(pointToAdd);
			
			if(isBisStep && !PointExistInStack(ref pointToAdd, bisStackPosition))
				bisStackPosition.Add(pointToAdd);
			
			stackByStep[currentStep].Add(pointToAdd);
		}

		public void setStep(int index, bool isBisStep)
		{
			bisStackPosition.Clear();
			this.isBisStep = isBisStep; 
			if (null == stackByStep)
				stackByStep = new List<List<Point>>();
			
			if(stackByStep.Count < index +1)
				stackByStep.Add(new List<Point>());

			currentStep = index;
		}

		public Point Next()
		{
			List<Point> stack;
			switch (option)
			{
				case EStackOption.USE_ONLY_SUB_STACK:
					stack = subStackPosition.Count > 0 ? subStackPosition : fullStackPosition;
					break;
				case EStackOption.USE_BOTH_STACK:
					stack = subStackPosition.Count > 0 && random.NextDouble() > FullStackUsageProbability ? subStackPosition : fullStackPosition;
					break;
				default:
					stack = fullStackPosition;
					break;
			}

			int index = random.Next(stack.Count);
			Point point = stack[index];
			RemovePoint(ref point);
			return point;
		}

		public void cleanUpBisAvailable()
		{
			bisStepStackPositionAvailableTop.Clear();
			bisStepStackPositionAvailableBottom.Clear();
			bisStepStackPositionAvailableLeft.Clear();
			bisStepStackPositionAvailableRight.Clear();
			
			bisStepStackPositionEmptyBottom.Clear();
			bisStepStackPositionEmptyTop.Clear();
			bisStepStackPositionEmptyLeft.Clear();
			bisStepStackPositionEmptyRight.Clear();

			bisStackPosition.Clear();
		}

		public StackType NextBis(out DoorDirection direction, out Point point)
		{
			direction = DoorDirection.UNDEFINED;
			point.X = -1;
			point.Y = -1;
			DoorDirection[] availableDirection =
				{DoorDirection.UP, DoorDirection.DOWN, DoorDirection.LEFT, DoorDirection.RIGHT};
			int intDirection = random.Next(availableDirection.Length);
			List<Point> stackEmpty = null;
			for (int i = 0; i < availableDirection.Length; i++)
			{
				direction = availableDirection[(intDirection + i) % availableDirection.Length];
				if(direction == DoorDirection.UP){
					if (bisStepStackPositionAvailableBottom.Count > 0 && bisStepStackPositionEmptyTop.Count > 0)
					{
						stackEmpty = bisStepStackPositionEmptyTop;
						break;
					}
				}else if (direction  ==  DoorDirection.DOWN)
				{
					if (bisStepStackPositionAvailableTop.Count > 0 && bisStepStackPositionEmptyBottom.Count > 0)
					{
						stackEmpty = bisStepStackPositionEmptyBottom;
						break;
					}
				}else if (direction ==  DoorDirection.LEFT)
				{
					if (bisStepStackPositionAvailableRight.Count > 0 && bisStepStackPositionEmptyLeft.Count > 0)
					{
						stackEmpty = bisStepStackPositionEmptyLeft;
						break;
					}
				}else if (direction ==  DoorDirection.RIGHT)
				{
					if (bisStepStackPositionAvailableLeft.Count > 0 && bisStepStackPositionEmptyRight.Count > 0)
					{
						stackEmpty = bisStepStackPositionEmptyRight;
						break;
					}
				}
			}
			if (null == stackEmpty)
			{
				if (bisStackPosition.Count > 0)
				{
					int indexbis = random.Next(bisStackPosition.Count);
					point = bisStackPosition[indexbis];
					RemovePoint(ref point);
					return StackType.STANDARD;	
				}
				
				if(stackByStep[currentStep].Count == 0)
					return StackType.NONE;
				
				int index = random.Next(stackByStep[currentStep].Count);
				point = stackByStep[currentStep][index];
				RemovePoint(ref point);
				return StackType.STANDARD;
			}
			
			int indexEmpty = random.Next(stackEmpty.Count);
			point = stackEmpty[indexEmpty];
			RemovePoint(ref point);
			return StackType.BIS;
		}

		public Point GetAvailableDoorWithDirection(DoorDirection direction)
		{
			List<Point> stack = null;
			if (direction == DoorDirection.UP)
				stack = bisStepStackPositionAvailableBottom;
			else if (direction == DoorDirection.DOWN)
				stack = bisStepStackPositionAvailableTop;
			else if (direction == DoorDirection.LEFT)
				stack = bisStepStackPositionAvailableRight;
			else
				stack = bisStepStackPositionAvailableLeft;

			int index = random.Next(stack.Count);
			Point p = stack[index];
			stack.RemoveAt(index);
			return p;
		}

		public void RemovePoint(ref Point coordinateToRemove)
		{
			int index = fullStackPosition.IndexOf(coordinateToRemove);
			if (index > -1)
				fullStackPosition.RemoveAt(index);

			index = subStackPosition.IndexOf(coordinateToRemove);
			if (index > -1)
				subStackPosition.RemoveAt(index);
			
			index = bisStepStackPositionEmptyTop.IndexOf(coordinateToRemove);
			if(index > -1)
				bisStepStackPositionEmptyTop.RemoveAt(index);
			
			index = bisStepStackPositionEmptyBottom.IndexOf(coordinateToRemove);
			if(index > -1)
				bisStepStackPositionEmptyBottom.RemoveAt(index);
			
			index = bisStepStackPositionEmptyLeft.IndexOf(coordinateToRemove);
			if(index > -1)
				bisStepStackPositionEmptyLeft.RemoveAt(index);
			
			index = bisStepStackPositionEmptyRight.IndexOf(coordinateToRemove);
			if(index > -1)
				bisStepStackPositionEmptyRight.RemoveAt(index);
			
			index = bisStackPosition.IndexOf(coordinateToRemove);
			if(index > -1)
				bisStackPosition.RemoveAt(index);

			for (int i = 0; i < stackByStep.Count; i++)
			{
				index = stackByStep[i].IndexOf(coordinateToRemove);
				if(index > -1)
					stackByStep[i].RemoveAt(index);
			}
		}

		public void SetOption(EStackOption option, ref Point origin)
		{
			this.option = option;
			if (option != EStackOption.USE_ONLY_FULL_STACK)
				subStackPosition = new List<Point> { origin };

		}

		bool PointExistInStack(ref Point p, List<Point> stack)
		{
			return stack.IndexOf(p) > -1;
		}

		public void AddTopDirectionDoorEmpty(ref Point point)
		{
			if(bisStepStackPositionEmptyTop.IndexOf(point) == -1)
				bisStepStackPositionEmptyTop.Add(point);
		}

		public void AddBottomDirectionDoorEmpty(ref Point point)
		{
			if(bisStepStackPositionEmptyBottom.IndexOf(point) == -1)
				bisStepStackPositionEmptyBottom.Add(point);
		}

		public void AddLeftDirectionDoorEmpty(ref Point point)
		{
			if(bisStepStackPositionEmptyLeft.IndexOf(point) == -1)
				bisStepStackPositionEmptyLeft.Add(point);
		}

		public void AddRightDirectionDoorEmpty(ref Point point)
		{
			if(bisStepStackPositionEmptyRight.IndexOf(point) == -1)
				bisStepStackPositionEmptyRight.Add(point);
		}

		public void AddTopDirectionDoorAvailable(ref Point point)
		{
			if(bisStepStackPositionAvailableTop.IndexOf(point) == -1)
				bisStepStackPositionAvailableTop.Add((point));
		}
		
		public void AddBottomDirectionDoorAvailable(ref Point point)
		{
			if(bisStepStackPositionAvailableBottom.IndexOf(point) == -1)
				bisStepStackPositionAvailableBottom.Add(point);
		}
		
		public void AddLeftDirectionDoorAvailable(ref Point point)
		{
			if(bisStepStackPositionAvailableLeft.IndexOf(point) == -1)
				bisStepStackPositionAvailableLeft.Add(point);
		}
		
		public void AddRightDirectionDoorAvailable(ref Point point)
		{
			if(bisStepStackPositionAvailableRight.IndexOf(point) == -1)
				bisStepStackPositionAvailableRight.Add(point);
		}

		int GetBisCount()
		{
			return Math.Min(bisStepStackPositionAvailableBottom.Count, bisStepStackPositionEmptyTop.Count) +
				Math.Min(bisStepStackPositionAvailableTop.Count, bisStepStackPositionEmptyBottom.Count) +
				Math.Min(bisStepStackPositionAvailableLeft.Count, bisStepStackPositionEmptyRight.Count) + 
			    Math.Min(bisStepStackPositionAvailableRight.Count, bisStepStackPositionEmptyLeft.Count);
		}
	}
}
